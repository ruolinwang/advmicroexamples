package ejm.adminclient;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import au.com.dius.pact.consumer.ConsumerPactTestMk2;
import au.com.dius.pact.consumer.MockServer;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.model.PactSpecVersion;
import au.com.dius.pact.model.RequestResponsePact;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.fest.assertions.Assertions;

import javax.ws.rs.core.MediaType;

import static org.fest.assertions.Assertions.assertThat;

/**
 * @author Ken Finnigan
 */
public class ConsumerPactTest extends ConsumerPactTestMk2 {
    public static enum TestCaseType {SINGLEGET, ALLGET, UPDATE, DELETE, CREATE}

    private static final TestCaseType testCaseType = TestCaseType.DELETE;

    private Category createCategory(Integer id, String name) {
        Category cat = new TestCategoryObject(id, LocalDateTime.parse("2002-01-01T00:00:00"), 1);
        cat.setName(name);
        cat.setVisible(Boolean.TRUE);
        cat.setHeader("header");
        cat.setImagePath("n/a");

        return cat;
    }

    @Override
    protected RequestResponsePact createPact(PactDslWithProvider builder) {

        List<Category> categoryList = new ArrayList<>();
        Category top = createCategory(0, "Top");
        categoryList.add(top);

        Category transport = createCategory(1000, "Transportation");
        transport.setParent(top);
        categoryList.add(transport);

        Category autos = createCategory(1002, "Automobiles");
        autos.setParent(transport);
        categoryList.add(autos);

        Category cars = createCategory(1009, "Cars");
        cars.setParent(autos);
        categoryList.add(cars);

        Category toyotas = createCategory(1015, "Toyota Cars");
        toyotas.setParent(cars);
        categoryList.add(toyotas);

        Category hondas = createCategory(1016, "Honda Cars");
        toyotas.setParent(cars);

        Category modifiedToyotas = createCategory(1015, "Toyota Models");
        modifiedToyotas.setParent(cars);

        ObjectMapper mapper = new ObjectMapper()
                .registerModule(new JavaTimeModule());

        try {
            if (testCaseType == TestCaseType.SINGLEGET) {
                return builder
                        .uponReceiving("Retrieve a category")
                        .path("/admin/category/1015")
                        .method("GET")
                        .willRespondWith()
                        .status(200)
                        .body(mapper.writeValueAsString(toyotas))
                        .toPact();
            } else if (testCaseType == TestCaseType.ALLGET) {
                return builder
                        .uponReceiving("Retrieve all categories")
                        .path("/admin/category")
                        .method("GET")
                        .willRespondWith()
                        .status(200)
                        .body(mapper.writeValueAsString(categoryList))
                        .toPact();
            } else if (testCaseType == TestCaseType.UPDATE) {
                return builder
                        .uponReceiving("Update a category")
                        .path("/admin/category/1015")
                        .body(mapper.writeValueAsString(modifiedToyotas),MediaType.APPLICATION_JSON)
                        .method("PUT")
                        .willRespondWith()
                        .status(200)
                        .body(mapper.writeValueAsString(modifiedToyotas))
                        .toPact();
            }
            else if (testCaseType == TestCaseType.DELETE) {
                return builder
                        .uponReceiving("Delete a category")
                        .path("/admin/category/1015")
                        .method("DELETE")
                        .willRespondWith()
                        .status(204)
                        .toPact();
            }
            else if (testCaseType == TestCaseType.CREATE) {
                return builder
                        .uponReceiving("Create a category")
                        .path("/admin/category")
                        .method("POST")
                        .body(mapper.writeValueAsString(hondas),MediaType.APPLICATION_JSON)
                        .willRespondWith()
                        .status(201)
                        .toPact();
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected String providerName() {
        return "admin_service_provider";
    }

    @Override
    protected String consumerName() {
        return "admin_client_consumer";
    }

    @Override
    protected PactSpecVersion getSpecificationVersion() {
        return PactSpecVersion.V3;
    }

    @Override
    protected void runTest(MockServer mockServer) throws IOException {


        if (testCaseType == TestCaseType.SINGLEGET) {
            Category cat = new AdminClient(mockServer.getUrl()).getCategory(1015);
            Assertions.assertThat(cat).isNotNull();
            assertThat(cat.getId()).isEqualTo(1015);
            assertThat(cat.getName()).isEqualTo("Toyota Cars");
            assertThat(cat.getHeader()).isEqualTo("header");
            assertThat(cat.getImagePath()).isEqualTo("n/a");
            assertThat(cat.isVisible()).isTrue();
            assertThat(cat.getParent()).isNotNull();
            assertThat(cat.getParent().getId()).isEqualTo(1009);
        } else if (testCaseType == TestCaseType.ALLGET) {
            List<Category> categories = new AdminClient(mockServer.getUrl()).getAllCategories();
            assertThat(categories.size() == 5);
            for (Category cat : categories) {
                if (cat.getId().equals(1015)) {
                    assertThat(cat.getName()).isEqualTo("Toyota Cars");
                }
            }
        } else if (testCaseType == TestCaseType.UPDATE) {
            List<Category> categoryList = new ArrayList<>();
            Category top = createCategory(0, "Top");
            categoryList.add(top);

            Category transport = createCategory(1000, "Transportation");
            transport.setParent(top);
            categoryList.add(transport);

            Category autos = createCategory(1002, "Automobiles");
            autos.setParent(transport);
            categoryList.add(autos);

            Category cars = createCategory(1009, "Cars");
            cars.setParent(autos);
            categoryList.add(cars);

            Category toyotas = createCategory(1015, "Toyota Cars");
            toyotas.setParent(cars);
            categoryList.add(toyotas);

            Category modifiedToyotas = createCategory(1015, "Toyota Models");
            modifiedToyotas.setParent(cars);
            Category cat = new AdminClient(mockServer.getUrl()).updateCategory(1015,modifiedToyotas );
            assertThat(cat.getName().equals("Toyota Models"));

        }
        else if (testCaseType == TestCaseType.DELETE) {
            String response = new AdminClient(mockServer.getUrl()).deleteCategory(1015);
            assertThat(response.split(" ")[1].equals("204"));
        }

        else if (testCaseType == TestCaseType.CREATE) {
            List<Category> categoryList = new ArrayList<>();
            Category top = createCategory(0, "Top");
            categoryList.add(top);

            Category transport = createCategory(1000, "Transportation");
            transport.setParent(top);
            categoryList.add(transport);

            Category autos = createCategory(1002, "Automobiles");
            autos.setParent(transport);
            categoryList.add(autos);

            Category cars = createCategory(1009, "Cars");
            cars.setParent(autos);
            categoryList.add(cars);

            Category toyotas = createCategory(1015, "Toyota Cars");
            toyotas.setParent(cars);
            categoryList.add(toyotas);

            Category hondas = createCategory(1016, "Honda Cars");
            toyotas.setParent(cars);

            Category modifiedToyotas = createCategory(1015, "Toyota Models");
            modifiedToyotas.setParent(cars);
            String response = new AdminClient(mockServer.getUrl()).createCategory(hondas);
            assertThat(response.split(" ")[1].equals("201"));
        }

    }
}
