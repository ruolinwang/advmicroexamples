package ejm.adminclient;

import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;

import javax.ws.rs.core.MediaType;

/**
 * @author Ken Finnigan
 */
public class AdminClient {
    private String url;

    public AdminClient(String url) {
        this.url = url;
    }

    public Category getCategory(final Integer categoryId) throws IOException {
        URIBuilder uriBuilder;
        try {
            uriBuilder = new URIBuilder(url).setPath("/admin/category/" + categoryId);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }

        String jsonResponse =
                Request
                    .Get(uriBuilder.toString())
                    .execute()
                        .returnContent().asString();

        if (jsonResponse.isEmpty()) {
            return null;
        }

        return new ObjectMapper()
                .registerModule(new JavaTimeModule())
                .readValue(jsonResponse, Category.class);
    }

    public List<Category> getAllCategories() throws IOException{
        URIBuilder uriBuilder;
        try {
            uriBuilder = new URIBuilder(url).setPath("/admin/category");
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }

        String jsonResponse =
                Request
                        .Get(uriBuilder.toString())
                        .execute()
                        .returnContent().asString();

        if (jsonResponse.isEmpty()) {
            return null;
        }

        return new ObjectMapper()
                .registerModule(new JavaTimeModule())
                .readValue(jsonResponse, new TypeReference<List<Category>>() { });
    }

    public Category updateCategory(final Integer categoryId, Category category) throws IOException{


        ObjectMapper mapper = new ObjectMapper()
                .registerModule(new JavaTimeModule());

        URIBuilder uriBuilder;
        try {
            uriBuilder = new URIBuilder(url).setPath("/admin/category/" + categoryId);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }

        String jsonResponse =
                Request
                        .Put(uriBuilder.toString())
                        .bodyString(mapper.writeValueAsString(category),ContentType.APPLICATION_JSON)
                        .execute()
                        .returnContent().asString();

        if (jsonResponse.isEmpty()) {
            return null;
        }

        return new ObjectMapper()
                .registerModule(new JavaTimeModule())
                .readValue(jsonResponse, Category.class);
    }


    public String deleteCategory(final Integer categoryId) throws IOException{
        URIBuilder uriBuilder;
        try {
            uriBuilder = new URIBuilder(url).setPath("/admin/category/" + categoryId);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }

        String jsonResponse =
                Request
                        .Delete(uriBuilder.toString())
                        .execute().returnResponse().getStatusLine().toString();

        if (jsonResponse.isEmpty()) {
            return null;
        }
        return jsonResponse;
    }

    public String createCategory(Category category) throws IOException {
        URIBuilder uriBuilder;
        try {
            uriBuilder = new URIBuilder(url).setPath("/admin/category" );
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        ObjectMapper mapper = new ObjectMapper()
                .registerModule(new JavaTimeModule());

        String jsonResponse =
                Request
                        .Post(uriBuilder.toString())
                        .bodyString(mapper.writeValueAsString(category),ContentType.APPLICATION_JSON)
                        .execute().returnResponse().getStatusLine().toString();

        if (jsonResponse.isEmpty()) {
            return null;
        }
        return jsonResponse;
    }
}
