package ejm.shopping;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author Ken Finnigan
 */
@ApplicationPath("/")
public class Chapter3ThornTailApplication extends javax.ws.rs.core.Application {
}